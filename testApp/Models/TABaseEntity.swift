//
//  TABaseEntity.swift
//  testApp
//
//  Created by Ivan on 8/29/18.
//  Copyright © 2018 Ivan. All rights reserved.
//

import UIKit

class TABaseEntity: NSObject {
    func convertToDict(json:NSString) -> NSDictionary? {
        let jsonText =  json
        var dictonary:NSDictionary?
        
        if let data = jsonText.data(using: String.Encoding.utf8.rawValue) {
            
            do {
                dictonary = try ((JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]) as NSDictionary?)
                
                if let myDictionary = dictonary
                {
                    return myDictionary
                    //  print(" First name is: \(((myDictionary["cast"] as! NSArray)[2] as! NSDictionary)["name"]!)")
                }
            } catch let error as NSError {
                return nil
                print(error)
            }
        } else {
            return nil
        }
        return nil
    }


}
