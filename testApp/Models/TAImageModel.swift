//
//  TAImageModel.swift
//  testApp
//
//  Created by Ivan on 8/30/18.
//  Copyright © 2018 Ivan. All rights reserved.
//

import UIKit

class TAImageModel: TABaseEntity {
    var imageData: UIImageView?
    var id: Int?
    var longitude: Float?
    var latitude: Float?
    var weather: NSString?
    var smallImagePath: NSString?
    var bigImagePath: NSString?
    
    convenience init(_ json: NSDictionary) {
        self.init()
        self.id = json["id"] as? Int
        self.bigImagePath = json["bigImagePath"] as? NSString
        self.smallImagePath = json["smallImagePath"] as? NSString
        self.longitude = (json["parameters"] as! NSDictionary)["longitude"] as? Float
        self.latitude = (json["parameters"] as! NSDictionary)["latitude"] as? Float
         self.weather = (json["parameters"] as! NSDictionary)["weather"] as? NSString
       
    
      //  let dictionary: NSDictionary = convertToDict(json: json)!
        print(json)
     //   self.tocken = dictionary["token"] as? String
     //   self.avatarLink = NSURL(string:(dictionary["avatar"] as? String)!)
        
        //print(" First name is: \(((myDictionary["cast"] as! NSArray)[2] as! NSDictionary)["name"]!)")
        
    }
    
}
