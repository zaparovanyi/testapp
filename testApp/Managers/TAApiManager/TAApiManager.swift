//
//  TAApiManager.swift
//  testApp
//
//  Created by Ivan on 8/29/18.
//  Copyright © 2018 Ivan. All rights reserved.
//

import UIKit
struct Constants {
    static let baseUrl = "http://api.doitserver.in.ua/"
}

class TAApiManager: NSObject {
    static let sharedInstance : TAApiManager = {
        let instance = TAApiManager()
        instance.manager.requestSerializer = AFJSONRequestSerializer()
        instance.manager.responseSerializer = AFHTTPResponseSerializer()
        return instance
    }()
    let manager = AFHTTPSessionManager()
    
    func methodPost(url:NSString, parameters: NSDictionary, responseArray: @escaping (NSString) -> Any, failure:@escaping (NSError)->Any) {
        //     let urlRes = Constants.baseUrl + (url as String)
        
        self.manager.post(NSString(format: "%@%@", Constants.baseUrl, url) as String, parameters: parameters, progress: nil, success: { (task: URLSessionDataTask, response:Any?) in
            let datastring = NSString(data: response as! Data, encoding: String.Encoding.utf8.rawValue)
            
            responseArray(datastring!)
        }) { (task: URLSessionDataTask?, error: Error) in
            NSLog("%@", "asas ")
            failure(error as NSError)
        }
        
    }
    
    func methodGet(url:NSString, parameters: NSDictionary?, responseArray: @escaping (NSString) -> Any, failure:@escaping (NSError)->Any) {
        self.manager.requestSerializer.setValue(TAUserSessionManager.sharedInstance.userModel?.tocken, forHTTPHeaderField: "token")
        self.manager.get(NSString(format: "%@%@", Constants.baseUrl, url) as String, parameters: parameters, progress: nil, success: { (task: URLSessionDataTask, response:Any?) in
            let datastring = NSString(data: response as! Data, encoding: String.Encoding.utf8.rawValue)
            
            responseArray(datastring!)
        }) { (task: URLSessionDataTask?, error: Error) in
            NSLog("%@", "asas ")
            failure(error as NSError)
        }
        
    }
    
    func uploadFile(file: NSData, url: String, parameters: NSDictionary, imageName:String, responseArray: @escaping (NSString) -> Any, failure:@escaping (NSError)->Any) {
        //     let urlRes = Constants.baseUrl + (url as String)
        self.manager.requestSerializer.setValue(TAUserSessionManager.sharedInstance.userModel?.tocken, forHTTPHeaderField: "token")
        manager.post(NSString(format: "%@%@", Constants.baseUrl, url) as String, parameters: parameters, constructingBodyWith: { (formData: AFMultipartFormData!) -> Void in
            formData.appendPart(withFileData: file as Data, name: imageName, fileName: "photo.jpg", mimeType: "image/jpeg")
        }, success: { (task: URLSessionDataTask, response:Any?) in
            let datastring = NSString(data: response as! Data, encoding: String.Encoding.utf8.rawValue)

            responseArray(datastring!)
        }) { (task: URLSessionDataTask?, error: Error) in
            NSLog("%@", "asas")
            
            
            failure(error as NSError)
            
        }
        
        
        
    }
}


