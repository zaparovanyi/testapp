//
//  TAApiManager+LoginRegister.swift
//  testApp
//
//  Created by Ivan on 8/29/18.
//  Copyright © 2018 Ivan. All rights reserved.
//

import UIKit

extension TAApiManager {
    func postLoginApp(login:NSString, password:NSString, responseArray: @escaping (NSString) -> Any, failure:@escaping (NSError)->Any) {
        let parameters: NSDictionary = ["email" : login,
                                        "password" : password]
        TAApiManager.sharedInstance.methodPost(url: "login", parameters: parameters, responseArray: { (response) -> Any in
            responseArray(response)
        }) { (error) -> Any in
           failure(error)
        }
        
    }
    
    func postRegisterApp(login:NSString, password:NSString, username:NSString, image: UIImage, responseArray: @escaping (NSString) -> Any, failure:@escaping (NSError)->Any) {
        let parameters: NSDictionary = ["email" : login,
                                        "password" : password,
                                        "username" : username]
        let imageData  : NSData = UIImageJPEGRepresentation(image as UIImage, 0.5)! as NSData
        TAApiManager.sharedInstance.uploadFile(file: imageData, url: "create", parameters: parameters,imageName: "avatar", responseArray: { (response) -> Any in
            responseArray(response)
        }) { (error) -> Any in
            failure(error)
        }
    }
    
}
