//
//  TAApiManager+Image.swift
//  testApp
//
//  Created by Ivan on 8/29/18.
//  Copyright © 2018 Ivan. All rights reserved.
//

import UIKit

extension TAApiManager  {
    func getAllImagesApp(login:NSString, password:NSString, responseArray: @escaping (NSString) -> Any, failure:@escaping (NSError)->Any) {

        TAApiManager.sharedInstance.methodGet(url: "all", parameters: nil, responseArray: { (response) -> Any in
            responseArray(response)
        }) { (error) -> Any in
            failure(error)
        }
        
    }
    
    func getGigApp(weather:NSString, responseArray: @escaping (NSString) -> Any, failure:@escaping (NSError)->Any) {
        let parameters: NSDictionary = ["weather" : weather]
        TAApiManager.sharedInstance.methodGet(url: "gif", parameters: parameters, responseArray: { (response) -> Any in
            responseArray(response)
        }) { (error) -> Any in
            failure(error)
        }
        
    }
    
    func uploadImageToServer(file:UIImage, description: NSString, hashtag:NSString, latitude:Float, longitude:Float,responseArray: @escaping (NSString) -> Any, failure:@escaping (NSError)->Any) {
         let imageData  : NSData = UIImageJPEGRepresentation(file as UIImage, 0.5)! as NSData
        let parameters: NSDictionary = ["description" : description,
                                        "hashtag" : hashtag,
                                        "latitude" : latitude,
                                        "longitude" : longitude]
        TAApiManager.sharedInstance.uploadFile(file: imageData, url: "image", parameters: parameters, imageName: "image", responseArray: { (response) -> Any in
            responseArray(response)
        }) { (error) -> Any in
            failure(error)
        }
    }
}
