//
//  TAAddImageViewController.swift
//  testApp
//
//  Created by Ivan on 8/30/18.
//  Copyright © 2018 Ivan. All rights reserved.
//
import CoreLocation
import UIKit

protocol didUploadImageDelegate {
    func didUploadImage()
}

class TAAddImageViewController: TABaseUIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate,CLLocationManagerDelegate {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var descriptionField: UITextField!
    @IBOutlet weak var hashteg: UITextField!
    var delegate:didUploadImageDelegate?
    var longitude:Float = 0.0
    var latitude: Float = 0.0
    var isImageSelected: Bool = false
    var locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.picker.delegate = self
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func addImageAction(_ sender: Any) {
        if (self.isImageSelected == true) {
        TAApiManager.sharedInstance.uploadImageToServer(file: self.imageView.image!, description: self.descriptionField.text! as NSString, hashtag: self.hashteg.text! as NSString, latitude: self.latitude, longitude: self.longitude, responseArray: { (response) -> Any in
            self.imageAdded()
        }) { (error) -> Any in
            print("")
        }
        } else {
            let alert = UIAlertController(title: "Alert", message: "You didnt choose image", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    func imageAdded() {
        self.delegate?.didUploadImage()
         self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func imagePickAction(_ sender: Any) {
        self.showPicker()
    }
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        self.imageView.image = chosenImage
        self.isImageSelected = true
        // use the image
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
   func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
 {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        self.longitude = Float(locValue.longitude)
        self.latitude = Float(locValue.latitude)
        print("locations = \(locValue.latitude) \(locValue.longitude)")
    }

}
