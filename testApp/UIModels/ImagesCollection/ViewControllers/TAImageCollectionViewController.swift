//
//  TAImageCollectionViewController.swift
//  testApp
//
//  Created by Ivan on 8/29/18.
//  Copyright © 2018 Ivan. All rights reserved.
//

import UIKit

class TAImageCollectionViewController: TABaseUIViewController, didUploadImageDelegate, UICollectionViewDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate,UICollectionViewDataSource {
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    var imagesArray: NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getImagesData()
        self.collectionView.delegate = self
        self.collectionView.dataSource = self 
   
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    @IBAction func addImageAction(_ sender: Any) {
        
        if let viewController:TAAddImageViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TAAddImageViewController") as? TAAddImageViewController {
            viewController.delegate = self
            if let navigator = navigationController {
                navigator.pushViewController(viewController, animated: true)
            }
        }
        
    }
    func errorM(data:Error) {
        let datastring = NSString(data: ((data as? NSError)?.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey]) as! Data, encoding: String.Encoding.utf8.rawValue)
        print(datastring as Any)
    }
    
    func getImagesData() {
        TAApiManager.sharedInstance.getAllImagesApp(login: " ", password: " ", responseArray: { (response) -> Any in
            self.getCollection(response: response)
        }) { (error) -> Any in
            self.errorM(data: error)
        }
    }
    
    func getCollection(response:NSString) {
        self.imagesArray.removeAllObjects()
        let dict:NSDictionary = TABaseEntity().convertToDict(json: response)!
        let arrayOfImages:NSArray = dict["images"] as! NSArray
        for i in 0..<arrayOfImages.count {
            let modelImage:TAImageModel = TAImageModel.init(arrayOfImages[i]  as! NSDictionary)
            
            self.imagesArray.add(modelImage)
        }
        self.collectionView.reloadData()
    }
    func didUploadImage() {
        self.getImagesData()
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.imagesArray.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let model:TAImageModel = (self.imagesArray[indexPath.row] as! TAImageModel)
        // get a reference to our storyboard cell
        let cell:TAImageCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "TAImageCollectionViewCell", for: indexPath as IndexPath) as! TAImageCollectionViewCell
        let url:String = model.smallImagePath! as String
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
        cell.imageView.downloaded(from:url)
        cell.weather.text = model.weather! as String
         cell.address.text = "Not set"
        cell.backgroundColor = UIColor.cyan // make cell more visible in our example project
        
        return cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
    }
    
    @IBAction func playGif(_ sender: Any) {
        if self.view.viewWithTag(100) != nil {
        }else{
            
            TAApiManager.sharedInstance.getGigApp(weather: "Cloudly", responseArray: { (response) -> Any in
                self.displayGif(gifString: response as String)
            }) { (error) -> Any in
                print(error)
            }
        }
    }
    
    func displayGif(gifString: String) {
        let dict:NSDictionary = TABaseEntity().convertToDict(json: gifString as NSString)! as NSDictionary
        let url: String = dict["gif"] as! String
        let frame:CGRect = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        let imageGif: TAGifImageView = TAGifImageView.instanceFromNib(frame: frame) as! TAGifImageView
        imageGif.tag = 100
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        imageGif.addGestureRecognizer(tap)
        let gifURL : String = url
        let imageURL = UIImage.gifImageWithURL(gifURL)
        imageGif.imageView.image = imageURL
        self.view.addSubview(imageGif)
    }
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        if let viewWithTag = self.view.viewWithTag(100) {
            viewWithTag.removeFromSuperview()
        }else{
            print("No!")
        }
    }
    
}
