//
//  TAGifImageView.swift
//  testApp
//
//  Created by Ivan on 8/30/18.
//  Copyright © 2018 Ivan. All rights reserved.
//

import UIKit

class TAGifImageView: UIView {
  
    @IBOutlet weak var imageView: UIImageView!
    class func instanceFromNib( frame:CGRect) -> UIView {
        let view  = UINib(nibName: "TAGifImageView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
        view.frame = CGRect(x: frame.origin.x, y: frame.origin.y, width: frame.size.width, height: frame.size.height) 
        return view
        
    }

    

    
    

}
