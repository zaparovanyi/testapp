//
//  TAImageCollectionViewCell.swift
//  testApp
//
//  Created by Ivan on 8/30/18.
//  Copyright © 2018 Ivan. All rights reserved.
//

import UIKit

class TAImageCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var weather: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var address: UILabel!
    
}
