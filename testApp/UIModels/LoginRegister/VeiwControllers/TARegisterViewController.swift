//
//  TARegisterViewController.swift
//  testApp
//
//  Created by Ivan on 8/29/18.
//  Copyright © 2018 Ivan. All rights reserved.
//

import UIKit

class TARegisterViewController: TABaseUIViewController,UIImagePickerControllerDelegate,
UINavigationControllerDelegate,handleErrorDelegate {
    @IBOutlet weak var registerView: UIView!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var loginView: UIView!
   
    
    var logView: UIView?
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        picker.delegate = self
        self.navigationController?.isNavigationBarHidden = true
        self.loginState = true
        // Do any additional setup after loading the view, typically from a nib.
    }
    var loginState: Bool = true {
        
        didSet {
            for view in self.loginView.subviews{
                view.removeFromSuperview()
            }
           
            if self.loginState {
                self.actionButton.setTitle("Register", for: .normal)
                self.logView =  TALoginView.instanceFromNibLogin(frame: self.loginView.frame)
                (self.logView as?TALoginView)?.delegate = self
                (self.logView as! TALoginView).compliteLoginRegister = {
                    print("Login Success!")
                    TAUserSessionManager.sharedInstance.loginState = true
                    let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "TAImageCollectionViewController") as! TAImageCollectionViewController
                    let navController = UINavigationController(rootViewController: VC1) // Creating a navigation controller with VC1 at the root of the navigation stack.
                    self.present(navController, animated:true, completion: nil)
                }
            } else {
                self.actionButton.setTitle("Login", for: .normal)
                self.logView =  TARegisterView.instanceFromNibLogin(frame: self.loginView.frame) as! TARegisterView
                (self.logView as?TARegisterView)?.delegate = self
                (self.logView as! TARegisterView).showImagePicker = {
                    () -> Void in
                    self.showPicker()
                    
                }
                (self.logView as! TARegisterView).compliteLoginRegister = {
                    print("Registered!")
                    TAUserSessionManager.sharedInstance.loginState = true
                    
                }
            }
            self.loginView.addSubview(self.logView!)
        }
        
    }
    @IBAction func changeState(_ sender: UIButton) {
        self.loginState = !self.loginState
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        (logView as! TARegisterView).avatarImage.image = chosenImage
        (logView as! TARegisterView).isPictureSelected = true
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func handleError(error:Error) {
         let datastring = NSString(data: ((error as? NSError)?.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey]) as! Data, encoding: String.Encoding.utf8.rawValue)
        let dict:NSDictionary = TABaseEntity().convertToDict(json: datastring!)!
        var messageError:String = ""
        if dict["error"] != nil {
            messageError = dict["error"] as! String
        } else if (dict["children"]) != nil {
        
        messageError = (((dict["children"] as! NSDictionary)["email"] as! NSDictionary)["errors"]! as! NSArray)[0] as! String
        }
        let alert = UIAlertController(title: "Alert", message: messageError, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
            
        
    }
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        self.view.endEditing(true)
    }
}

