//
//  TABaseUIViewController.swift
//  testApp
//
//  Created by Ivan on 8/30/18.
//  Copyright © 2018 Ivan. All rights reserved.
//

import UIKit

class TABaseUIViewController: UIViewController {
    let picker = UIImagePickerController()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showPicker() {
        self.picker.allowsEditing = false
        self.picker.sourceType = .photoLibrary
        self.present(self.picker, animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
