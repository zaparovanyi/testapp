//
//  TALoginView.swift
//  testApp
//
//  Created by Ivan on 8/29/18.
//  Copyright © 2018 Ivan. All rights reserved.
//

import UIKit

class TALoginView: TABaseLoginView {
    class func instanceFromNibLogin( frame:CGRect) -> UIView {
        let view  = UINib(nibName: "TALoginXib", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
        view.frame = CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height)
        return view
        
    }
    
    @IBAction func LoginAction(_ sender: Any) {
        if ((self.email.text?.count)!>5 && (self.password.text?.count)!>4) {
            
            TAApiManager.sharedInstance.postLoginApp(login:self.email.text! as NSString, password:self.password.text! as NSString, responseArray: { (response) -> Any in
                self.convertToModel(json: response)
            }) { (error) -> Any in
                self.delegate?.handleError(error: error)
            }
        } else {
            let data = "{\"error\":\"Email or password is too short\"}".data(using: .utf8)
            let errorTemp = NSError(domain:"", code:1002, userInfo: [AFNetworkingOperationFailingURLResponseDataErrorKey : data as Any])
            
            self.delegate?.handleError(error: errorTemp)
        }
    }
    
    
}
