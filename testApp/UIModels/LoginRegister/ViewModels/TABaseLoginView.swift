//
//  TABaseLoginView.swift
//  testApp
//
//  Created by Ivan on 8/29/18.
//  Copyright © 2018 Ivan. All rights reserved.
//

import UIKit
protocol handleErrorDelegate {
    func handleError(error:Error)
}

class TABaseLoginView: UIView {
    var delegate:handleErrorDelegate?
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var email: UITextField!
        var compliteLoginRegister: (() -> Void)?
    func convertToModel(json:NSString) {
        let userModel:TAUserModel? = TAUserModel.init(json)
        if ((userModel) != nil) {
            TAUserSessionManager.sharedInstance.userModel = userModel
            if let callback = self.compliteLoginRegister {
                callback()
            }
        }
        print("")
    }

}
