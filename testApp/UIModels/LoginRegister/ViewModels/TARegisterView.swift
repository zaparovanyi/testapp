//
//  TARegisterView.swift
//  testApp
//
//  Created by Ivan on 8/29/18.
//  Copyright © 2018 Ivan. All rights reserved.
//

import UIKit

class TARegisterView: TABaseLoginView {
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var avatarImage: UIImageView!
    var isPictureSelected:Bool =  false
    var showImagePicker: (() -> Void)?
    class func instanceFromNibLogin( frame:CGRect) -> UIView {
        let view  = UINib(nibName: "TARegisterXib", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
          view.frame = CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height)
        return view
        
    }
    @IBAction func registerAction(_ sender: Any) {
       if ((self.email.text?.count)!>5 && (self.password.text?.count)!>4 && (self.isPictureSelected == true)) {
        TAApiManager.sharedInstance.postRegisterApp(login: self.email.text! as NSString, password: self.password.text as! NSString, username: self.userName.text as! NSString, image: self.avatarImage.image!, responseArray: { (response:NSString) -> Any in
            self.convertToModel(json: response)
        }) { (error:NSError) -> Any in
            //print("")
            self.delegate?.handleError(error: error)
        }
       } else {
        let data = "{\"error\":\"Email or password is too short. Or you didnt choose picture\"}".data(using: .utf8)
        let errorTemp = NSError(domain:"", code:1002, userInfo: [AFNetworkingOperationFailingURLResponseDataErrorKey : data as Any])
        
        self.delegate?.handleError(error: errorTemp)
        }
    }
    @IBOutlet weak var addImage: UIButton!
    
    @IBAction func addImageAction(_ sender: Any) {
        if let callback = self.showImagePicker {
            callback()
        }
    }
    
    

}
